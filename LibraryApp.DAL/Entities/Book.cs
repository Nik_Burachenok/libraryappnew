﻿namespace LibraryApp.DAL.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Text.RegularExpressions;

    public class Book
    {
        public int Id
        {
            get; set;
        }

        public string Title
        {
            get; set;
        }

        public string Isbn
        {
            get; set;
        }

        public string Publisher
        {
            get; set;
        }
        public int TotalCount
        {
            get; set;
        }
        public int Count
        {
            get; set;
        }
        public string Discription
        {
            get; set;
        }
        public Author Author
        {
            get; set;
        }
        public int AuthorId
        {
            get; set;
        }
        public IEnumerable<Order> Order
        {
            get; set;
        }
        public byte[] Picture
        {
            get; set;
        }
        public bool IsFavoriteBook
        {
            get; set;
        }
    }
}

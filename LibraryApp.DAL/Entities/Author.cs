﻿namespace LibraryApp.DAL.Entities
{
    using System.Collections.Generic;

    public class Author
    {
        public int Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string Discription
        {
            get; set;
        }

        public IEnumerable<Book> Book
        {
            get; set;
        }

        public byte[] Picture
        {
            get; set;
        }
    }
}

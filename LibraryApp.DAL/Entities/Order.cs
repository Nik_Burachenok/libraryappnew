﻿namespace LibraryApp.DAL.Entities
{
    using Infrastructure;
    using System;

    public class Order
    {
        public int Id
        {
            get; set;
        }
        public Guid UserId
        {
            get; set;
        }
        public User User
        {
            get; set;
        }
        public int BookId
        {
            get; set;
        }
        public Book Book
        {
            get; set;
        }
        public int Count
        {
            get; set;
        }
        public Status Status
        {
            get; set;
        }
        public DateTime DateOrder
        {
            get; set;
        }
        public DateTime DataCheckout
        {
            get; set;
        }
        public DateTime DateReturn
        {
            get; set;
        }
    }
}

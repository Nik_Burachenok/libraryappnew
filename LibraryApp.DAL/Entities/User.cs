﻿namespace LibraryApp.DAL.Entities
{
    using Microsoft.AspNetCore.Identity;
    using System;
    using System.Collections.Generic;

    public class User : IdentityUser<Guid>
    {
        public IEnumerable<Order> Order
        {
            get; set;
        }
    }
}

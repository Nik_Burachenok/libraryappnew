﻿using LibraryApp.DAL.EF;
using LibraryApp.DAL.Entities;
using LibraryApp.DAL.interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryApp.DAL.repositories
{
    public class RoleRepository : IRoleRepository
    {
        private LibraryContext db;

        public RoleRepository(LibraryContext context)
        {
            this.db = context;
        }
        public List<IdentityRole<Guid>> Get()
        {
            return this.db.Roles.ToList();
        }

        public void Create(string name)
        {
            this.db.Roles.Add(new IdentityRole<Guid> { Name = name, NormalizedName = name.ToUpper() });
            this.db.SaveChanges();
        }

        public IdentityRole<Guid> GetById(Guid id)
        {
            return this.db.Roles.Find(id);
        }

        public void Delete(IdentityRole<Guid> role)
        {
            this.db.Roles.Remove(role);
            this.db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.db.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

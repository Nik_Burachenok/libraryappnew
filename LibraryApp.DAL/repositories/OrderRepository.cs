﻿using LibraryApp.DAL.EF;
using LibraryApp.DAL.Entities;
using LibraryApp.DAL.interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryApp.DAL.repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly LibraryContext db;

        public OrderRepository(LibraryContext context)
        {
            this.db = context;
        }

        public int Create(Order order)
        {
            this.db.Orders.Add(order);
            this.db.SaveChanges();
            var result = order.Id;
            return result;
        }

        public void Update(Order order)
        {
            this.db.Entry(order).State = EntityState.Modified;
            this.db.SaveChanges();
        }

        public IEnumerable<Order> Get()
        {
            return this.db.Orders.Include(x => x.User).Include(x => x.Book).ToList();
        }

        public IEnumerable<Order> GetOnQuery(int query)
        {
            return this.db.Orders.Include(x => x.User).Include(x => x.Book).Where(order => order.Id == query).ToList();
        }

        public Order GetById(int id)
        {
            return this.db.Orders.Include(x => x.User).Include(x => x.Book).FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Order> GetCheckout()
        {
            return this.db.Orders.Include(x => x.User).Include(x => x.Book).Where(order => (int)order.Status == 2).ToList();
        }
        public IEnumerable<Order> GetUnprocessed()
        {
            return this.db.Orders.Include(x => x.User).Include(x => x.Book).Where(order => (int)order.Status == 1).ToList();
        }

        public IEnumerable<Order> GetReturnOrder()
        {
            return this.db.Orders.Include(x => x.User).Include(x => x.Book).Where(order => (int)order.Status == 3).ToList();
        }

        public IEnumerable<Order> GetListOrdersUser(Guid userId)
        {
            return this.db.Orders.Include(x => x.Book).Where(order => order.UserId == userId).Where(order => (int)order.Status == 1 || (int)order.Status == 2).ToList();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.db.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

﻿using LibraryApp.DAL.EF;
using LibraryApp.DAL.Entities;
using LibraryApp.DAL.interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryApp.DAL.repositories
{
    public class BookRepository : IBookRepository
    {
        private readonly LibraryContext db;

        public BookRepository(LibraryContext context)
        {
            this.db = context;
        }
        public int Create(Book book)
        {
            this.db.Books.Add(book);
            this.db.SaveChanges();
            var result = book.Id;
            return result;
        }

        public void Delete(int id)
        {
            Book book = this.db.Books.Find(id);
            if (book != null)
            {
                this.db.Books.Remove(book);
            }
            this.db.SaveChanges();
        }

        public (int, IEnumerable<Book>) Get(int pageNumber, int pageSize)
        {
            var books = this.db.Books.Skip(pageSize * (pageNumber - 1)).Take(pageSize).ToList();
            var count = this.db.Books.Count();
            var result = (count, books);
            return result;
        }

        public IEnumerable<Book> GetIsFavorite()
        {
            return this.db.Books.Where(x => x.IsFavoriteBook == true).ToList();
        }

        public Book GetById(int id)
        {
            return this.db.Books.Include(x => x.Author).FirstOrDefault(x => x.Id == id);
        }

        public Book GetByIdAsNoTracking(int id)
        {
            return this.db.Books.AsNoTracking().Include(x => x.Author).FirstOrDefault(x => x.Id == id);
        }

        public (int, IEnumerable<Book>) GetByIsbn(string isbn, int pageNumber, int pageSize)
        {
            var books = this.db.Books.Where(book => book.Isbn == isbn)
                           .Skip(pageSize * (pageNumber - 1))
                           .Take(pageSize)
                           .ToList();
            var count = this.db.Books.Where(book => book.Isbn == isbn).ToList().Count();
            var result = (count, books);
            return result;
        }

        public (int, IEnumerable<Book>) GetByTitle(string titlePart, int pageNumber, int pageSize)
        {
            var books = this.db.Books.Where(book => book.Title.Contains(titlePart))
                                              .Skip(pageSize * (pageNumber - 1))
                                              .Take(pageSize)
                                              .ToList();
            var count = this.db.Books.Where(book => book.Title.Contains(titlePart)).ToList().Count();
            var result = (count, books);
            return result;
        }

        public (int, IEnumerable<Book>) GetByAuthor(int authorId, int pageNumber, int pageSize)
        {
            var books = this.db.Books.Where(book => book.AuthorId == authorId)
                           .Skip(pageSize * (pageNumber - 1))
                           .Take(pageSize)
                           .ToList();
            var count = this.db.Books.Where(book => book.AuthorId == authorId).ToList().Count();
            var result = (count, books);
            return result;
        }

        public void Update(Book book)
        {
            this.db.Entry(book).State = EntityState.Modified;
            this.db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                   this.db.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

﻿using LibraryApp.DAL.EF;
using LibraryApp.DAL.Entities;
using LibraryApp.DAL.interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryApp.DAL.repositories
{
    public class AuthorRepository : IAuthorRepository
    {
        private readonly LibraryContext db;

        public AuthorRepository(LibraryContext context)
        {
            this.db = context;
        }
        public int Create(Author author)
        {
            this.db.Authors.Add(author);
            this.db.SaveChanges();
            var result = author.Id;
            return result;
        }

        public void Delete(int id)
        {
            Author author = this.db.Authors.Find(id);

            if (author != null)
            {
                this.db.Authors.Remove(author);
            }
            this.db.SaveChanges();
        }

        public (int, IEnumerable<Author>) Get(int pageNumber, int pageSize)
        {
            var authors = this.db.Authors.Skip(pageSize * (pageNumber - 1)).Take(pageSize).ToList();
            var count = this.db.Authors.Count();
            var result = (count, authors);
            return result;
        }

        public IEnumerable<Author> GetAll()
        {
            return this.db.Authors.ToList();
        }

        public Author GetById(int id)
        {
            return this.db.Authors.Include(x => x.Book).FirstOrDefault(x => x.Id == id); 
        }

        public Author GetByIdAsNoTracking(int id)
        {
            return this.db.Authors.AsNoTracking().Include(x => x.Book).FirstOrDefault(x => x.Id == id);
        }

        public (int, IEnumerable<Author>) GetByQuery(string query, int pageNumber, int pageSize)
        {
            var authors = this.db.Authors.Where(author => author.Name.Contains(query))
                                              .Skip(pageSize * (pageNumber - 1))
                                              .Take(pageSize)
                                              .ToList();
            var count = this.db.Authors.Where(author => author.Name.Contains(query)).ToList().Count();
            var result = (count, authors);
            return result;
        }

        public void Update(Author author)
        {
            this.db.Entry(author).State = EntityState.Modified;
            this.db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.db.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

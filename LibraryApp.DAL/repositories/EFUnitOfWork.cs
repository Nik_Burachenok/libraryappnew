﻿using LibraryApp.DAL.EF;
using LibraryApp.DAL.Entities;
using LibraryApp.DAL.interfaces;
using Microsoft.AspNetCore.Identity;
using System;

namespace LibraryApp.DAL.repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly LibraryContext db;
        private BookRepository bookRepository;
        private AuthorRepository authorRepository;
        private UserRepository userRepository;
        private RoleRepository roleRepository;
        private OrderRepository orderRepository;
        private readonly PasswordHasher<User> passwordHasher;

        public EFUnitOfWork(LibraryContext context)
        { 
            this.db = context; 
        }


        public IBookRepository Book
        {
            get
            {
                if (this.bookRepository == null)
                {
                    this.bookRepository = new BookRepository(this.db);
                }
                return this.bookRepository;
            }

            set
            {
                this.bookRepository = (BookRepository)value;
            }
        }

        public IAuthorRepository Author
        {
            get
            {
                if (this.authorRepository == null)
                {
                    this.authorRepository = new AuthorRepository(this.db);
                }
                return this.authorRepository;
            }

            set
            {
                this.authorRepository = (AuthorRepository)value;
            }
        }

        public IUserRepository UserModel
        {
            get
            {
                if (this.userRepository == null)
                {
                    this.userRepository = new UserRepository(this.db, this.passwordHasher);
                }
                return this.userRepository;
            }

            set
            {
                this.userRepository = (UserRepository)value;
            }
        }

        public IRoleRepository Role
        {
            get
            {
                if (this.roleRepository == null)
                {
                    this.roleRepository = new RoleRepository(this.db);
                }
                return this.roleRepository;
            }

            set
            {
                this.roleRepository = (RoleRepository)value;
            }
        }

        public IOrderRepository Order
        {
            get
            {
                if (this.orderRepository == null)
                {
                    this.orderRepository = new OrderRepository(this.db);
                }
                return this.orderRepository;
            }

            set
            {
                this.orderRepository = (OrderRepository)value;
            }
        }

        public bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (this.disposed)
                {
                    this.db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            this.db.SaveChanges();
        }
    }
}

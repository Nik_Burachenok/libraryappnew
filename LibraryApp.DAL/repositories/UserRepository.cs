﻿namespace LibraryApp.DAL.repositories
{
    using LibraryApp.DAL.EF;
    using LibraryApp.DAL.Entities;
    using LibraryApp.DAL.interfaces;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;


    public class UserRepository : IUserRepository/*, IPasswordHasher<User>*/
    {

        private readonly LibraryContext db;
        private readonly PasswordHasher<User> passwordHasher;

        public UserRepository(LibraryContext context, PasswordHasher<User> passwordHasher)
        {
            this.db = context;
            this.passwordHasher = passwordHasher;
        }

        #region Hide

        //public async Task<IdentityResult> Create(UserModel model, UserManager<User> userManager, SignInManager<User> signInManager)
        //{
        //    User user = new User()
        //    {
        //        UserName = model.Name,
        //        Email = model.Email,
        //    };
        //    var result = await userManager.CreateAsync(user, model.Password);
        //    await db.SaveChangesAsync();
        //    await signInManager.SignInAsync(user, false);
        //    return result;
        //}

        //public async Task<SignInResult> Login(UserModel model, SignInManager<User> signInManager)
        //{
        //    var result = await signInManager.PasswordSignInAsync(model.Name, model.Password, model.RememberMe, false);
        //    return result;
        //}

        //public (int, IEnumerable<UserModel>) Get(UserManager<User> userManager, int pageNumber, int pageSize)
        //{
        //    var users = userManager.Users.Skip(pageSize * (pageNumber - 1)).Take(pageSize).Select(user => new UserModel
        //    {
        //        Id = user.Id,
        //        Name = user.UserName,
        //        Email = user.Email
        //    }).ToList();
        //    var count = userManager.Users.Count();
        //    var result = (count, users);
        //    return result;
        //}

        //public IEnumerable<UserModel> GetByQuery(UserManager<User> userManager, string query)
        //{
        //    return userManager.Users.Where(model => model.UserName.Contains(query))
        //                            .Select(user => new UserModel
        //                            {
        //                                Id = user.Id,
        //                                Name = user.UserName,
        //                                Email = user.Email
        //                            }).ToList();
        //}

        //public async Task<UserModel> GetById(UserManager<User> userManager, string id)
        //{
        //    var model = await userManager.FindByIdAsync(id);
        //    return new UserModel
        //    {
        //        Id = model.Id,
        //        Name = model.UserName,
        //        Email = model.Email
        //    };
        //}

        //public async Task<IdentityResult> Delete(string id, UserManager<User> userManager)
        //{
        //    var user = await userManager.FindByIdAsync(id);
        //    var result = await userManager.DeleteAsync(user);
        //    await db.SaveChangesAsync();
        //    return result;
        //}

        //public async Task<IdentityResult> Edit(UserManager<User> userManager, UserModel model)
        //{
        //    var user = new User
        //    {
        //        Id = model.Id,
        //        UserName = model.Name,
        //        Email = model.Email
        //    };
        //    var result = await userManager.UpdateAsync(user);
        //    return result;
        //}

        //public async Task<IdentityResult> ChangePassword(UserManager<User> userManager, UserModel model, string oldPassword, string newPassword)
        //{
        //    var dalUser = this.db.Users.FirstOrDefault(x => x.Id == model.Id);



        //    var result = await userManager.ChangePasswordAsync(dalUser, oldPassword, newPassword);
        //    return result;
        //}


        #endregion

        #region IUserStore
        public async Task<IdentityResult> CreateAsync(User user, CancellationToken cancellationToken)
        {
            try
            {
                this.db.Users.Add(user);
                await this.db.SaveChangesAsync(cancellationToken);

                return IdentityResult.Success;
            }
            catch (Exception ex)
            {
                return IdentityResult.Failed(
                    new IdentityError
                    {
                        Code = ex.GetType().Name,
                        Description = ex.Message
                    });
            }
        }

        public async Task<IdentityResult> CreateAsync(User user, string password, CancellationToken cancellationToken)
        {
            try
            {
                var hashpassword = this.passwordHasher.HashPassword(user, password);
                user.PasswordHash = hashpassword;
                this.db.Users.Add(user);
                await this.db.SaveChangesAsync(cancellationToken);

                return IdentityResult.Success;
            }
            catch (Exception ex)
            {
                return IdentityResult.Failed(
                    new IdentityError
                    {
                        Code = ex.GetType().Name,
                        Description = ex.Message
                    });
            }
        }

        public async Task<IdentityResult> DeleteAsync(User user, CancellationToken cancellationToken)
        {
            try
            {
                this.db.Users.Remove(user);
                await this.db.SaveChangesAsync(cancellationToken);

                return IdentityResult.Success;
            }
            catch (Exception ex)
            {
                return IdentityResult.Failed(
                    new IdentityError
                    {
                        Code = ex.GetType().Name,
                        Description = ex.Message
                    });
            }
        }

        public async Task<User> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            return await this.db.Users.FindAsync(Guid.Parse(userId));
        }

        public async Task<User> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            return await this.db.Users.FirstOrDefaultAsync(user => user.NormalizedUserName == normalizedUserName);
        }

        public async Task<string> GetNormalizedUserNameAsync(User user, CancellationToken cancellationToken)
        {
            return await Task.FromResult(user.NormalizedUserName);
        }

        public async Task<string> GetUserIdAsync(User user, CancellationToken cancellationToken)
        {
            return await Task.FromResult(user.Id.ToString());
        }

        public async Task<string> GetUserNameAsync(User user, CancellationToken cancellationToken)
        {
            return await Task.FromResult(user.UserName);
        }

        public Task SetNormalizedUserNameAsync(User user, string normalizedName, CancellationToken cancellationToken)
        {
            user.NormalizedUserName = normalizedName;
            this.db.Update(user);

            return Task.FromResult(0);
        }

        public Task SetUserNameAsync(User user, string userName, CancellationToken cancellationToken)
        {
            user.UserName = userName;
            this.db.Update(user);

            return Task.FromResult(0);
        }

        public async Task<IdentityResult> UpdateAsync(User user, CancellationToken cancellationToken)
        {
            try
            {
                this.db.Update(user);
                //await this.db.SaveChangesAsync(cancellationToken);

                return IdentityResult.Success;
            }
            catch (Exception ex)
            {
                return IdentityResult.Failed(
                    new IdentityError
                    {
                        Code = ex.GetType().Name,
                        Description = ex.Message
                    });
            }
        }
        #endregion

        #region IUserRoleStore
        public Task AddToRoleAsync(User user, string roleName, CancellationToken cancellationToken)
        {
            var role = this.db.Roles.First(x => x.Name == roleName);
            this.db.UserRoles.Add(new UserRole
            {
                UserId = user.Id,
                RoleId = role.Id
            });

            this.db.SaveChangesAsync(cancellationToken);
            return Task.FromResult(0);
        }

        public Task RemoveFromRoleAsync(User user, string roleName, CancellationToken cancellationToken)
        {
            var role = this.db.Roles.First(x => x.Name == roleName);
            var userRole = this.db.UserRoles.First(x => x.RoleId == role.Id && x.UserId == user.Id);
            this.db.UserRoles.Remove(userRole);
            this.db.SaveChangesAsync(cancellationToken);
            return Task.FromResult(0);
        }

        public Task<IList<string>> GetRolesAsync(User user, CancellationToken cancellationToken)
        {
            var userRoles = this.db.UserRoles.Where(x => x.UserId == user.Id).ToList();
            var roles = new List<IdentityRole<Guid>>();
            foreach (var userRole in userRoles)
            {
                roles.Add(this.db.Roles.Find(userRole.RoleId));
            }
            IList<string> rolesName = new List<string>();
            foreach (var role in roles)
            {
                rolesName.Add(role.Name);
            }
            return Task.FromResult(rolesName);
        }

        public async Task<bool> IsInRoleAsync(User user, string roleName, CancellationToken cancellationToken)
        {
            var roles = await this.GetRolesAsync(user, cancellationToken);
            foreach (var role in roles)
            {
                if (role == roleName)
                {
                    return true;
                }
            }
            return false;
        }

        public Task<IList<User>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            var role = this.db.Roles.First(x => x.Name == roleName);
            var userRoles = this.db.UserRoles.Where(x => x.RoleId == role.Id).ToList();
            IList<User> users = new List<User>();
            foreach (var userId in userRoles)
            {
                users.Add(this.db.Users.Find(userId));
            }
            return Task.FromResult(users);
        }
        #endregion

        #region IUserPasswordStore
        public Task SetPasswordHashAsync(User user, string passwordHash, CancellationToken cancellationToken)
        {
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        public Task<string> GetPasswordHashAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PasswordHash != null);
        }
        #endregion


        public (int, IEnumerable<User>) Get(int pageNumber, int pageSize)
        {
            var users = this.db.Users.Skip(pageSize * (pageNumber - 1)).Take(pageSize).Select(user => new User
            {
                Id = user.Id,
                UserName = user.UserName,
                Email = user.Email
            }).ToList();
            var count = this.db.Users.Count();
            var result = (count, users);
            return result;
        }

        public IEnumerable<User> GetByQuery(string query)
        {
            return this.db.Users.Where(user => user.UserName.Contains(query))
                .Select(user => new User
                {
                    Id = user.Id,
                    UserName = user.UserName,
                    Email = user.Email
                }).ToList();
        }

        public async Task SetPasswordHash(User user, string passwordHash, CancellationToken cancellationToken)
        {
            user.PasswordHash = passwordHash;
            await this.UpdateAsync(user, cancellationToken);
        }
        public string GetPasswordHash(User user, CancellationToken cancellationToken)
        {
            var model = this.FindByIdAsync(user.Id.ToString(), cancellationToken);
            return model.Result.PasswordHash;
        }

        #region Disposable

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.db.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }


        #endregion Disposable
    }
}

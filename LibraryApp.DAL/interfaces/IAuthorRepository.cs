﻿using LibraryApp.DAL.Entities;
using System;
using System.Collections.Generic;

namespace LibraryApp.DAL.interfaces
{
    public interface IAuthorRepository : IDisposable
    {
        (int, IEnumerable<Author>) GetByQuery(string query, int pageNumber, int pageSize);
        IEnumerable<Author> GetAll();
        (int, IEnumerable<Author>) Get(int pageNumber, int pageSize);
        Author GetById(int id);
        Author GetByIdAsNoTracking(int id);
        int Create(Author item);
        void Update(Author item);
        void Delete(int id);
    }
}

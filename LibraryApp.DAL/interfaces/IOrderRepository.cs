﻿using LibraryApp.DAL.Entities;
using System;
using System.Collections.Generic;

namespace LibraryApp.DAL.interfaces
{
    public interface IOrderRepository : IDisposable
    {
        int Create(Order order);
        void Update(Order order);
        Order GetById(int id);
        IEnumerable<Order> Get();
        IEnumerable<Order> GetOnQuery(int query);
        IEnumerable<Order> GetCheckout();
        IEnumerable<Order> GetUnprocessed();
        IEnumerable<Order> GetReturnOrder();
        IEnumerable<Order> GetListOrdersUser(Guid userId);
    }
}

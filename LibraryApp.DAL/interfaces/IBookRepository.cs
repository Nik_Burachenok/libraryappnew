﻿using LibraryApp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryApp.DAL.interfaces
{
    public interface IBookRepository : IDisposable
    {
        (int, IEnumerable<Book>) GetByIsbn(string isbn, int pageNumber, int pageSize);
        (int, IEnumerable<Book>) GetByTitle(string title, int pageNumber, int pageSize);
        (int, IEnumerable<Book>) GetByAuthor(int authorId, int pageNumber, int pageSize);
        IEnumerable<Book> GetIsFavorite();
        (int, IEnumerable<Book>) Get(int pageNumber, int pageSize);
        Book GetById(int id);
        Book GetByIdAsNoTracking(int id);
        int Create(Book item);
        void Update(Book item);
        void Delete(int id);

    }
}

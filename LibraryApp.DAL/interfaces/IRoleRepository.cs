﻿using LibraryApp.DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LibraryApp.DAL.interfaces
{
    public interface IRoleRepository : IDisposable
    {
        List<IdentityRole<Guid>> Get();
        void Create(string name);
        IdentityRole<Guid> GetById(Guid id);
        void Delete(IdentityRole<Guid> role);
    }
}

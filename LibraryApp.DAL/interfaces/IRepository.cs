﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryApp.DAL.interfaces
{
    public interface IRepository<T> : IDisposable
        where T : class
    {
        IEnumerable<T> Get(int pageNumber, int pageSize);
        T GetById(int id);
        int Create(T item);
        void Update(T item);
        void Delete(int id);
    }
}

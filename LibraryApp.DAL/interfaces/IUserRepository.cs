﻿namespace LibraryApp.DAL.interfaces
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using LibraryApp.DAL.Entities;
    using Microsoft.AspNetCore.Identity;

    public interface IUserRepository : IUserStore<User>, IUserPasswordStore<User>, IUserRoleStore<User>
    {
        (int, IEnumerable<User>) Get(int pageNumber, int pageSize);
        IEnumerable<User> GetByQuery(string query);
        string GetPasswordHash(User user, CancellationToken cancellationToken);
        Task SetPasswordHash(User user, string passwordHash, CancellationToken cancellationToken);
        Task<IdentityResult> CreateAsync(User user, string password, CancellationToken cancellationToken);
    }
}

﻿using LibraryApp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryApp.DAL.interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IBookRepository Book { get; set; }
        IAuthorRepository Author { get; set; }
        IUserRepository UserModel { get; set; }
        IRoleRepository Role { get; set; }
        IOrderRepository Order { get; set; }
        void Save();
    }
}

﻿namespace LibraryApp.DAL.EF
{
    using LibraryApp.DAL.Entities;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using System;

    public class LibraryContext
        : IdentityDbContext<User, IdentityRole<Guid>, Guid>
    {
        public DbSet<Book> Books
        {
            get; set;
        }
        public DbSet<Author> Authors
        {
            get; set;
        }
        public DbSet<Order> Orders
        {
            get; set;
        }


        public LibraryContext(DbContextOptions<LibraryContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Book>()
                .HasOne(x => x.Author)
                .WithMany(x => x.Book)
                .HasForeignKey(x => x.AuthorId)
                .HasPrincipalKey(x => x.Id)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Order>()
                .HasOne(x => x.User)
                .WithMany(x => x.Order)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Order>()
                .HasOne(x => x.Book)
                .WithMany(x => x.Order)
                .HasForeignKey(x => x.BookId)
                .OnDelete(DeleteBehavior.Restrict);

            Guid admin_ID = Guid.NewGuid();
            Guid role_ID = admin_ID;

            modelBuilder.Entity<IdentityRole<Guid>>()
                .HasData(new IdentityRole<Guid>
                {
                    Id = role_ID,
                    Name = "admin",
                    NormalizedName = "admin"
                });

            var hasher = new PasswordHasher<User>();

            modelBuilder.Entity<User>()
                .HasData(new User
                {
                    Id = admin_ID,
                    UserName = "Admin",
                    NormalizedUserName = "Admin".ToUpper(),
                    Email = "admin@gmail.com",
                    NormalizedEmail = "admin@gmail.com".ToUpper(),
                    EmailConfirmed = true,
                    PasswordHash = hasher.HashPassword(null, "admin1"),
                    SecurityStamp = string.Empty
                });

            modelBuilder.Entity<IdentityUserRole<Guid>>()
                .HasData(new IdentityUserRole<Guid>
                {
                    RoleId = role_ID,
                    UserId = admin_ID
                });
        }
    }
}

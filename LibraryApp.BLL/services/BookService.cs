﻿using LibraryApp.BLL.DTO.Author;
using LibraryApp.BLL.DTO.Book;
using LibraryApp.BLL.DTO.Common;
using LibraryApp.BLL.interfaces;
using LibraryApp.DAL.Entities;
using LibraryApp.DAL.interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LibraryApp.BLL.services
{
    public class BookService : IBookService
    {
        private readonly IUnitOfWork uow;

        public BookService(IUnitOfWork uow)
        {
            this.uow = uow;
        }
        public PaginatedList<BookItem> Get(int pageNumber, int pageSize)
        {
            var model = this.uow.Book.Get(pageNumber, pageSize);
            var count = model.Item1;
            var books = model.Item2;
            return new PaginatedList<BookItem>(count, pageNumber, pageSize)
            {
                Items = books.Select(book => new BookItem
                                {
                                    Id = book.Id,
                                    Title = book.Title,
                                    Isbn = book.Isbn,
                                    Publisher = book.Publisher,
                                    TotalCount = book.TotalCount,
                                    Count = book.Count,
                                    Discription = book.Discription,
                                    Picture = book.Picture
                                })
            };
        }

        public IEnumerable<BookItem> GetIsFavorite()
        {
            return this.uow.Book.GetIsFavorite()
                .Select(x => new BookItem
                {
                    Id = x.Id,
                    Title = x.Title,
                    Publisher = x.Publisher,
                    IsFavoriteBook = x.IsFavoriteBook,
                    Isbn = x.Isbn,
                    TotalCount = x.TotalCount,
                    Count = x.Count,
                    Discription = x.Discription,
                    Picture = x.Picture
                });
        }

        public PaginatedList<BookItem> GetOnQuery(string query, int pageNumber, int pageSize)
        {
            if (BookItem.IsIsbn(query))
            {
                var model1 = this.uow.Book.GetByIsbn(query, pageNumber, pageSize);
                var count1 = model1.Item1;
                var books1 = model1.Item2;
                return new PaginatedList<BookItem>(count1, pageNumber, pageSize)
                {
                    Items = books1.Select(book => new BookItem
                                    {
                                        Id = book.Id,
                                        Title = book.Title,
                                        Isbn = book.Isbn,
                                        Publisher = book.Publisher,
                                        TotalCount = book.TotalCount,
                                        Count = book.Count,
                                        Discription = book.Discription,
                                        Picture = book.Picture
                                    })
                };
            }

            var model2 = this.uow.Book.GetByTitle(query, pageNumber, pageSize);
            var count2 = model2.Item1;
            var books2 = model2.Item2;
            return new PaginatedList<BookItem>(count2, pageNumber, pageSize)
            {
                Items = books2.Select(book => new BookItem
                                {
                                    Id = book.Id,
                                    Title = book.Title,
                                    Isbn = book.Isbn,
                                    Publisher = book.Publisher,
                                    TotalCount = book.TotalCount,
                                    Count = book.Count,
                                    Discription = book.Discription,
                                    Picture = book.Picture
                                })
            };
        }

        public BookItem GetById(int id)
        {
            var book = this.uow.Book.GetById(id);
            return new BookItem
            {
                Id = book.Id,
                Title = book.Title,
                Isbn = book.Isbn,
                Discription = book.Discription,
                Publisher = book.Publisher,
                TotalCount = book.TotalCount,
                Count = book.Count,
                Picture = book.Picture,
                IsFavoriteBook = book.IsFavoriteBook,
                AuthorItem = new AuthorItem
                {
                    Id = book.Author.Id,
                    Name = book.Author.Name,
                }
            };
        }

        public PaginatedList<BookItem> GetByAuthor(int authorId, int pageNumber, int pageSize)
        {
            var model = this.uow.Book.GetByAuthor(authorId, pageNumber, pageSize);
            var count = model.Item1;
            var books = model.Item2;
            return new PaginatedList<BookItem>(count, pageNumber, pageSize)
            {
                Items = books.Select(book => new BookItem
                                {
                                    Id = book.Id,
                                    Title = book.Title,
                                    Isbn = book.Isbn,
                                    Publisher = book.Publisher,
                                    TotalCount = book.TotalCount,
                                    Count = book.Count,
                                    Discription = book.Discription,
                                    Picture = book.Picture
                                })
            };
        }

        public int Create (BookItem model)
        {

            if (model.AuthorItem != null)
            {
                Book book = new Book
                {
                    Title = model.Title.Trim(),
                    Isbn = model.Isbn,
                    Publisher = model.Publisher.Trim(),
                    TotalCount = model.TotalCount,
                    Count = model.Count,
                    Discription = model.Discription.Trim(),
                    AuthorId = model.AuthorItem.Id
                };

                if (model.Image != null)
                {
                    byte[] imageData = null;

                    using (var binaryReader = new BinaryReader(model.Image.OpenReadStream()))
                    {
                        imageData = binaryReader.ReadBytes((int)model.Image.Length);
                    }

                    book.Picture = imageData;
                }

                return this.uow.Book.Create(book);
            }
            else
            {
                throw new Exception("Поле Выберите автора' не может быть пустым. При отсутствии необходимого автора в перечне, создайте его модель в разделе 'Авторы'");
            }
            
        }

        public void Delete (int id)
        {
            this.uow.Book.Delete(id);
        }

        public void Update(BookItem model)
        {
            var book = new Book
            {
                Id = model.Id,
                Title = model.Title.Trim(),
                Isbn = model.Isbn,
                Publisher = model.Publisher.Trim(),
                Count = model.Count,
                TotalCount = model.TotalCount,
                Discription = model.Discription.Trim(),
                AuthorId = model.AuthorItem.Id
            };
            if (model.Image != null)
            {
                byte[] imageData = null;

                using (var binaryReader = new BinaryReader(model.Image.OpenReadStream()))
                {
                    imageData = binaryReader.ReadBytes((int)model.Image.Length);
                }

                book.Picture = imageData;
            }
            else
            {
                var oldBook = this.uow.Book.GetByIdAsNoTracking(model.Id);
                book.Picture = oldBook.Picture;
            }
            //if (model.Pic != null)
            //{
            //    author.Picture = Convert.FromBase64String(model.Pic);
            //}

            this.uow.Book.Update(book);
        }

        public void MakePopular(int bookId)
        {
            var book = this.uow.Book.GetById(bookId);
            if (book.IsFavoriteBook)
            {
                book.IsFavoriteBook = false;
            }
            else
            {
                book.IsFavoriteBook = true;
            }
            this.uow.Book.Update(book);
        }
    }
}

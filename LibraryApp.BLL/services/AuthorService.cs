﻿using LibraryApp.BLL.DTO.Author;
using LibraryApp.BLL.DTO.Book;
using LibraryApp.BLL.DTO.Common;
using LibraryApp.BLL.interfaces;
using LibraryApp.DAL.Entities;
using LibraryApp.DAL.interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LibraryApp.BLL.services
{
    public class AuthorService : IAuthorService
    {
        private readonly IUnitOfWork uow;

        public AuthorService(IUnitOfWork uow)
        {
            this.uow = uow;
        }
        public PaginatedList<AuthorItem> Get(int pageNumber, int pageSize)
        {
            var model = this.uow.Author.Get(pageNumber, pageSize);
            var count = model.Item1;
            var authors = model.Item2;
            return new PaginatedList<AuthorItem>(count, pageNumber, pageSize)
            {
                Items = authors.Select(author => new AuthorItem
                {
                    Id = author.Id,
                    Name = author.Name,
                    Picture = author.Picture
                })
        };
        }

        public IEnumerable<AuthorItem> GetAll()
        {
            return this.uow.Author.GetAll()
                             .Select(author => new AuthorItem
                             {
                                 Id = author.Id,
                                 Name = author.Name
                             });
        }

        public AuthorItem GetById(int id)
        {
            var author = this.uow.Author.GetById(id);
            return new AuthorItem()
            {
                Id = author.Id,
                Name = author.Name,
                Discription = author.Discription,
                Picture = author.Picture,
                Book = author.Book.Select((x, i) =>
                {
                    return new BookItem
                    {
                        Id = x.Id,
                        Title = x.Title,
                        Publisher = x.Publisher,
                        Count = x.Count
                    };
                }).ToList()
            };
        }

        public PaginatedList<AuthorItem> GetByQuery(string query, int pageNumber, int pageSize)
        {
            var model = this.uow.Author.GetByQuery(query, pageNumber, pageSize);
            var count = model.Item1;
            var authors = model.Item2;
            return new PaginatedList<AuthorItem>(count, pageNumber, pageSize)
            {
                Items = authors.Select(author => new AuthorItem
                                {
                                    Id = author.Id,
                                    Name = author.Name,
                                    Discription = author.Discription,
                                    Picture = author.Picture
                                })
            };
        }

        public int Create(AuthorItem model)
        {
            Author author = new Author
            {
                Name = model.Name.Trim(),
                Discription = model.Discription.Trim(),
            };
            if (model.Image != null)
            {
                byte[] imageData = null;

                using (var binaryReader = new BinaryReader(model.Image.OpenReadStream()))
                {
                    imageData = binaryReader.ReadBytes((int)model.Image.Length);
                }

                author.Picture = imageData;
            }
            return this.uow.Author.Create(author);
        }

        public void Delete(int id)
        {
            this.uow.Author.Delete(id);
        }

        public void Update(AuthorItem model)
        {
            var author = new Author
            {
                Id = model.Id,
                Name = model.Name.Trim(),
                Discription = model.Discription.Trim()
            };
            if (model.Image != null)
            {
                byte[] imageData = null;

                using (var binaryReader = new BinaryReader(model.Image.OpenReadStream()))
                {
                    imageData = binaryReader.ReadBytes((int)model.Image.Length);
                }

                author.Picture = imageData;
            }
            else
            {
                var oldAuthor = this.uow.Author.GetByIdAsNoTracking(model.Id);
                author.Picture = oldAuthor.Picture;
            }
            //if (model.Pic != null)
            //{
            //    author.Picture = Convert.FromBase64String(model.Pic);
            //}
            this.uow.Author.Update(author);
        }
    }
}

﻿using LibraryApp.BLL.DTO.Common;
using LibraryApp.BLL.interfaces;
using LibraryApp.DAL.interfaces;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Linq;

namespace LibraryApp.BLL.services
{
    public class CartService : ICartService
    {
        private readonly IUnitOfWork uow;
        private readonly ISession session;

        public CartService(IUnitOfWork uow, IHttpContextAccessor contextAccessor)
        {
            this.uow = uow;
            this.session = contextAccessor?.HttpContext.Session;
        }

        public Cart AddCartItem(CartItem item)
        {
            var cart = this.Get();
            if (cart.CartItems.Any(x => x.BookId == item.BookId))
            {
                var book = this.uow.Book.GetById(item.BookId);
                if (book.Count >= item.Count && item.Count > 0)
                {
                    cart.CartItems.First(x => x.BookId == item.BookId).Count = item.Count;
                }
                else if (item.Count > 0 && book.Count < item.Count)
                {
                    throw new ArgumentOutOfRangeException("В библиотеле отсутствует указанное количество книг");
                }
                else
                {
                    throw new ArgumentNullException("Вы ввели некорректное значение количества книг");
                }
            }
            else
            {
                cart.CartItems.Add(item);
            }

            this.session.SetString("Cart", JsonConvert.SerializeObject(cart));

            return cart;
        }

        public void Delete(int bookId)
        {
            var cart = this.Get();

            var item = cart.CartItems.FirstOrDefault(x => x.BookId == bookId);
            cart.CartItems.Remove(item);
            this.session.SetString("Cart", JsonConvert.SerializeObject(cart));
        }

        public Cart Get()
        {
            Cart cart;
            try
            {
                cart = JsonConvert.DeserializeObject<Cart>(this.session.GetString("Cart"));
            }
            catch (Exception)
            {
                cart = new Cart { Id = Guid.NewGuid().ToString() };
                this.session.SetString("Cart", JsonConvert.SerializeObject(cart));
            }
            return cart;
        }
    }
}

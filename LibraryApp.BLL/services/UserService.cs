﻿using LibraryApp.BLL.DTO.Common;
using LibraryApp.BLL.DTO.Identity;
using LibraryApp.BLL.DTO.Role;
using LibraryApp.BLL.DTO.User;
using LibraryApp.BLL.interfaces;
using LibraryApp.DAL.Entities;
using LibraryApp.DAL.interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryApp.BLL.services
{
    public class UserService : IUserService
    {
        private readonly UserManager<User> userManager;
        private readonly IUserRepository userRepository;
        private readonly SignInManager<User> signInManager;
        private readonly PasswordHasher<User> passwordHasher;

        public UserService(UserManager<User> userManager, IUserRepository userRepository, SignInManager<User> signInManager, PasswordHasher<User> passwordHasher)
        {
            this.userManager = userManager;
            this.userRepository = userRepository;
            this.signInManager = signInManager;
            this.passwordHasher = passwordHasher;
        }

        public async Task<SignInResult> Login(UserBLL user)
        {
            return await this.signInManager.PasswordSignInAsync(user.UserName, user.Password, false, false);

        }

        public async Task<IdentityResult> Create(UserBLL model)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = cancellationTokenSource.Token;
            var user = new User()
            {
                UserName = model.UserName,
                Email = model.Email
            };
            return await this.userManager.CreateAsync(user, model.Password);
            //return await this.userRepository.CreateAsync(user, model.Password, cancellationToken);
        }

        public async void Logout()
        {
            await this.signInManager.SignOutAsync();
        }

        public PaginatedList<UserListItem> Get(int pageNumber, int pageSize)
        {
            var model = this.userRepository.Get(pageNumber, pageSize);
            var count = model.Item1;
            var users = model.Item2;
            return new PaginatedList<UserListItem>(count, pageNumber, pageSize)
            {
                Items = users.Select(user => new UserListItem
                {
                    Id = user.Id,
                    Name = user.UserName,
                    Email = user.Email
                })
            };
        }

        public IEnumerable<UserListItem> GetByQuery(string query)
        {
            return this.userRepository.GetByQuery(query)
                                .Select(user => new UserListItem
                                {
                                    Id = user.Id,
                                    Name = user.UserName,
                                    Email = user.Email
                                }).ToList();
        }

        public async Task<UserListItem> GetById(Guid id)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = cancellationTokenSource.Token;

            var userId = id.ToString();
            var user = await this.userRepository.FindByIdAsync(userId, cancellationToken);
            return new UserListItem
            {
                Id = user.Id,
                Name = user.UserName,
                Email = user.Email
            };
        }

        public async Task<IdentityResult> Delete(Guid id)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = cancellationTokenSource.Token;

            var userId = id.ToString();
            var user = await this.userRepository.FindByIdAsync(userId, cancellationToken);
            var result = await this.userRepository.DeleteAsync(user, cancellationToken);
            return result;
        }

        public async Task<IdentityResult> Edit(UserListItem model)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = cancellationTokenSource.Token;
            var user = new User
            {
                Id = model.Id,
                UserName = model.Name,
                Email = model.Email
            };
            var result = await this.userRepository.UpdateAsync(user, cancellationToken);
            return result;
        }

        public async Task ChangePassword(UserAndPasswordModel model)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = cancellationTokenSource.Token;
            var user = new User
            {
                Id = model.Id,
                UserName = model.Name
            };
            var currentHash = this.userRepository.GetPasswordHash(user, cancellationToken);
            var result = this.passwordHasher.VerifyHashedPassword(user, currentHash, model.OldPassword);

            if (result == PasswordVerificationResult.Success || result == PasswordVerificationResult.SuccessRehashNeeded)
            {
                var newHashPassword = this.passwordHasher.HashPassword(user, model.NewPassword);
                await this.userRepository.SetPasswordHash(user, newHashPassword, cancellationToken);
            }
            else
            {
                throw new Exception("Вы ввели неверный старый пароль");
            }
            
        }

        public async Task<IList<string>> GetUserRoles(Guid id)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = cancellationTokenSource.Token;
            var userId = id.ToString();
            var modelUser = this.GetById(id);
            var user = new User
            {
                Id = modelUser.Result.Id,
                UserName = modelUser.Result.Name,
                Email = modelUser.Result.Email
            };
            return await this.userRepository.GetRolesAsync(user, cancellationToken);
        }
    }
}

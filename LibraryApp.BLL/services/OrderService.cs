﻿using LibraryApp.BLL.DTO.Book;
using LibraryApp.BLL.DTO.Common;
using LibraryApp.BLL.DTO.Identity;
using LibraryApp.BLL.DTO.Order;
using LibraryApp.BLL.interfaces;
using LibraryApp.DAL.Entities;
using LibraryApp.DAL.interfaces;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryApp.BLL.services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork uow;
        private readonly ISession session;

        public OrderService (IUnitOfWork uow, IHttpContextAccessor contextAccessor)
        {
            this.uow = uow;
            this.session = contextAccessor?.HttpContext.Session;
        }

        public void Checkout(int orderId)
        {
            var order = this.uow.Order.GetById(orderId);
            order.Status = (Infrastructure.Status)2;
            order.DataCheckout = DateTime.Now;
            this.uow.Order.Update(order);
        }

        public void ReturnBook(int orderId)
        {
            var order = this.uow.Order.GetById(orderId);
            order.Status = (Infrastructure.Status)3;
            order.DateReturn = DateTime.Now;
            this.uow.Order.Update(order);
            var book = this.uow.Book.GetById(order.Book.Id);
            book.Count += order.Count;
            this.uow.Book.Update(book);
        }

        public List<int> Create(Guid userId)
        {
            var cart = JsonConvert.DeserializeObject<Cart>(this.session.GetString("Cart"));
            var numbersOrders = new List<int>();
            foreach (var cartItem in cart.CartItems)
            {
                var order = new Order
                {
                    BookId = cartItem.BookId,
                    Count = cartItem.Count,
                    UserId = userId,
                    Status = (Infrastructure.Status)1,
                    DateOrder = DateTime.Now
                };
                var orderId = this.uow.Order.Create(order);
                numbersOrders.Add(orderId);
                var book = this.uow.Book.GetById(cartItem.BookId);
                book.Count -= cartItem.Count;
                this.uow.Book.Update(book);
            }
            this.session.Clear();

            return numbersOrders;
        }

        public IEnumerable<OrderBLL> Get()
        {
            return this.uow.Order.Get()
                .Select(order => new OrderBLL
                {
                    Id = order.Id,
                    User =  new UserListItem
                        {
                            Id = order.User.Id,
                            Name = order.User.UserName,
                            Email = order.User.Email
                        },
                    Book = new BookItem
                    {
                        Id = order.Book.Id,
                        Title = order.Book.Title,
                        Isbn = order.Book.Isbn
                    },
                    Count = order.Count,
                    DateOrder = order.DateOrder,
                    DateCheckout = order.DataCheckout,
                    Status = (int)order.Status,
                    DateReturn = order.DateReturn
                }); 
        }

        public IEnumerable<OrderBLL> GetOnQuery(string query)
        {
            if (int.TryParse(query, out var result))
            {
                var orders = this.uow.Order.GetOnQuery(result);

                if (orders != null)
                {
                    var resultOrders = orders.Select(order => new OrderBLL
                    {
                        Id = order.Id,
                        User = new UserListItem
                        {
                            Id = order.User.Id,
                            Name = order.User.UserName,
                            Email = order.User.Email
                        },
                        Book = new BookItem
                        {
                            Id = order.Book.Id,
                            Title = order.Book.Title,
                            Isbn = order.Book.Isbn
                        },
                        Count = order.Count,
                        DateOrder = order.DateOrder,
                        DateCheckout = order.DataCheckout,
                        Status = (int)order.Status
                    });
                    return resultOrders;
                }
                else
                {
                    throw new ArgumentNullException();
                }
            }
            else
            {
                throw new Exception("Вы ввели некорректное значение номера заказа");
            }
            
        }

        public OrderBLL GetById(int id)
        {
            var order = this.uow.Order.GetById(id);
            return new OrderBLL
            {
                Id = order.Id,
                User = new UserListItem
                {
                    Id = order.User.Id,
                    Name = order.User.UserName,
                    Email = order.User.Email
                },
                Book = new BookItem
                {
                    Id = order.Book.Id,
                    Title = order.Book.Title,
                    Isbn = order.Book.Isbn
                },
                Count = order.Count,
                DateOrder = order.DateOrder,
                Status = (int)order.Status
            };
        }

        public IEnumerable<OrderBLL> GetCheckout()
        {
            return this.uow.Order.GetCheckout()
                .Select(order => new OrderBLL
                {
                    Id = order.Id,
                    User = new UserListItem
                    {
                        Id = order.User.Id,
                        Name = order.User.UserName,
                        Email = order.User.Email
                    },
                    Book = new BookItem
                    {
                        Id = order.Book.Id,
                        Title = order.Book.Title,
                        Isbn = order.Book.Isbn
                    },
                    Count = order.Count,
                    DateOrder = order.DateOrder,
                    DateCheckout = order.DataCheckout,
                    Status = (int)order.Status,
                    DateReturn = order.DateReturn
                });
        }

        public IEnumerable<OrderBLL> GetUnprocessed()
        {
            return this.uow.Order.GetUnprocessed()
                .Select(order => new OrderBLL
                {
                    Id = order.Id,
                    User = new UserListItem
                    {
                        Id = order.User.Id,
                        Name = order.User.UserName,
                        Email = order.User.Email
                    },
                    Book = new BookItem
                    {
                        Id = order.Book.Id,
                        Title = order.Book.Title,
                        Isbn = order.Book.Isbn
                    },
                    Count = order.Count,
                    DateOrder = order.DateOrder,
                    Status = (int)order.Status,
                    DateReturn = order.DateReturn
                });
        }

        public IEnumerable<OrderBLL> GetReturnOrder()
        {
            return this.uow.Order.GetReturnOrder()
                .Select(order => new OrderBLL
                {
                    Id = order.Id,
                    User = new UserListItem
                    {
                        Id = order.User.Id,
                        Name = order.User.UserName,
                        Email = order.User.Email
                    },
                    Book = new BookItem
                    {
                        Id = order.Book.Id,
                        Title = order.Book.Title,
                        Isbn = order.Book.Isbn
                    },
                    Count = order.Count,
                    DateOrder = order.DateOrder,
                    Status = (int)order.Status,
                    DateReturn = order.DateReturn
                });
        }

        public IEnumerable<OrderBLL> GetListOrdersUser(Guid userId)
        {
            return this.uow.Order.GetListOrdersUser(userId)
                .Select(order => new OrderBLL
                {
                    Id = order.Id,
                    Book = new BookItem
                    {
                        Id = order.Book.Id,
                        Title = order.Book.Title,
                        Isbn = order.Book.Isbn
                    },
                    Status = (int)order.Status,
                    Count = order.Count,
                    DateOrder = order.DateOrder,
                    DateCheckout = order.DataCheckout
                });
        }
    }
}

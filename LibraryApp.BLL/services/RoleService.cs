﻿using LibraryApp.BLL.DTO.Role;
using LibraryApp.BLL.interfaces;
using LibraryApp.DAL.Entities;
using LibraryApp.DAL.interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryApp.BLL.services
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork uow;
        private readonly UserManager<User> userManager;

        public RoleService(IUnitOfWork uow, UserManager<User> userManager)
        {
            this.uow = uow;
            this.userManager = userManager;
        }

        public void Create(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                this.uow.Role.Create(name);
            }
            else
            {
                throw new Exception("Заполните поле названия роли");
            }
        }

        public void Delete(RoleBLL role)
        {
            this.uow.Role.Delete(new IdentityRole<Guid> {
                Id = role.Id,
                Name = role.Name
            });
        }

        public List<RoleBLL> Get()
        {
            return this.uow.Role.Get().Select(role => new RoleBLL {
                Id = role.Id,
                Name = role.Name
            }).ToList();
        }

        public RoleBLL GetById(Guid id)
        {
            var role = this.uow.Role.GetById(id);
            return new RoleBLL
            {
                Id = role.Id,
                Name = role.Name
            };
        }

        public async Task Edit(Guid userId, List<string> roles)
        {
            var id = userId.ToString(); 
            User user = await this.userManager.FindByIdAsync(id);
            var userRoles = await this.userManager.GetRolesAsync(user);
            var addedRoles = roles.Except(userRoles);
            var removedRoles = userRoles.Except(roles);
            await this.userManager.AddToRolesAsync(user, addedRoles);
            await this.userManager.RemoveFromRolesAsync(user, removedRoles);
        }
    }
}

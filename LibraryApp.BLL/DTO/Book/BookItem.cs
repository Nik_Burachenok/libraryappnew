﻿using LibraryApp.BLL.DTO.Author;
using Microsoft.AspNetCore.Http;
using System.Text.RegularExpressions;

namespace LibraryApp.BLL.DTO.Book
{
    public class BookItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Isbn { get; set; }
        public string Publisher { get; set; }
        public int TotalCount { get; set; }
        public int Count { get; set; }
        public string Discription { get; set; }
        public AuthorItem AuthorItem { get; set; }
        public byte[] Picture { get; set; }
        public string Pic
        {
            get; set;
        }
        public IFormFile Image { get; set; }
        public bool IsFavoriteBook { get; set; }

        internal static bool IsIsbn(string s)
        {
            if (s == null)
            {
                return false;
            }

            s = s.Replace("-", "")
                 .Replace(" ", "")
                 .ToUpper();

            return Regex.IsMatch(s, @"^ISBN\d{10}(\d{3})?$");
        }
    }
}

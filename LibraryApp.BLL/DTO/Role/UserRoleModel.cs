﻿using System;
using System.Collections.Generic;

namespace LibraryApp.BLL.DTO.Role
{
    public class UserRoleModel
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public IList<string> UserRoles { get; set; }
    }
}

﻿namespace LibraryApp.BLL.DTO.Role
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class RoleBLL
    {
        public Guid Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }
    }
}

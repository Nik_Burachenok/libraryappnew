﻿using LibraryApp.BLL.DTO.Book;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryApp.BLL.DTO.Author
{
    public class AuthorItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Discription { get; set; }
        public IEnumerable<BookItem> Book { get; set; }
        public IFormFile Image { get; set; }
        public byte[] Picture { get; set; }
        public string Pic
        {
            get; set;
        }
    }
}

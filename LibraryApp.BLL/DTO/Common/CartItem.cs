﻿namespace LibraryApp.BLL.DTO.Common
{
    public class CartItem
    {
        public int BookId { get; set; }
        public string BookTitle { get; set; }
        public string AuthorName { get; set; }
        public int Count { get; set; } = 1;
    }
}

﻿using LibraryApp.DAL.interfaces;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryApp.BLL.DTO.Common
{
    public class Cart
    {
        public Cart()
        {

        }

        public string Id { get; set; }

        public IList<CartItem> CartItems { get; set; } = new List<CartItem>();
        public int TotalCount
        {
            get { return this.CartItems.Sum(items => items.Count); }
        }
    }
}

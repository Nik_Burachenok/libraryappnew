﻿using System;
using System.Collections.Generic;

namespace LibraryApp.BLL.DTO.Common
{
    public class PaginatedList<T>
        where T : class
    {
        public int PageNumber { get; private set; }

        public int PageSize { get; private set; }

        public int TotalPage { get; set; }

        public IEnumerable<T> Items { get; set; }

        public PaginatedList(int count, int pageNumber, int pageSize)
        {
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.TotalPage = (int)Math.Ceiling((double)count / pageSize);

        }

        public bool HasPreviousPage
        {
            get
            {
                return (this.PageNumber > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (this.PageNumber < this.TotalPage);
            }
        }
    }
}

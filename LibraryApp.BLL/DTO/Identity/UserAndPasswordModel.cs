﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryApp.BLL.DTO.Identity
{
    public class UserAndPasswordModel
    {
        public Guid Id
        {
            get; set;
        }
        public string Name
        {
            get; set;
        }
        public string Email
        {
            get; set;
        }
        public string NewPassword
        {
            get; set;
        }
        public string OldPassword
        {
            get; set;
        }
    }
}

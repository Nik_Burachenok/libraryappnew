﻿using LibraryApp.BLL.DTO.Book;
using LibraryApp.BLL.DTO.Identity;
using System;

namespace LibraryApp.BLL.DTO.Order
{
    public class OrderBLL
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public UserListItem User { get; set; }
        public int BookId { get; set; }
        public BookItem Book { get; set; }
        public int Count { get; set; }
        public DateTime DateOrder { get; set; }
        public DateTime DateCheckout { get; set; }
        public int Status { get; set; }
        public  DateTime DateReturn { get; set; }
    }
}

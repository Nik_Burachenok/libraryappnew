﻿using LibraryApp.BLL.DTO.Common;
using LibraryApp.BLL.DTO.Identity;
using LibraryApp.BLL.DTO.User;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LibraryApp.BLL.interfaces
{
    public interface IUserService
    {
        //PaginatedList<UserListItem> Get(UserManager<User> userManager, int pageNumber, int pageSize);
        //Task<IdentityResult> Edit(UserManager<User> userManager, UserListItem model);
        //Task<IdentityResult> ChangePassword(UserManager<User> userManager, UserListItem model, string oldPassword, string newPassword);
        Task<SignInResult> Login(UserBLL user);
        Task<IdentityResult> Create(UserBLL model);
        void Logout();
        PaginatedList<UserListItem> Get(int pageNumber, int pageSize);
        IEnumerable<UserListItem> GetByQuery(string query);
        Task<UserListItem> GetById(Guid id);
        Task<IdentityResult> Delete(Guid id);
        Task<IdentityResult> Edit(UserListItem model);
        Task ChangePassword(UserAndPasswordModel model);
        Task<IList<string>> GetUserRoles(Guid id);
    }
}

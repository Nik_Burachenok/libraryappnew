﻿using LibraryApp.BLL.DTO.Author;
using LibraryApp.BLL.DTO.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryApp.BLL.interfaces
{
    public interface IAuthorService
    {
        PaginatedList<AuthorItem> Get(int pageNumber, int pageSize);
        IEnumerable<AuthorItem> GetAll();
        AuthorItem GetById(int id);
        PaginatedList<AuthorItem> GetByQuery(string query, int pageNumber, int pageSize);
        int Create(AuthorItem model);
        void Delete(int id);
        void Update(AuthorItem model);
    }
}

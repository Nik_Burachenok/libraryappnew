﻿using LibraryApp.BLL.DTO.Role;
using LibraryApp.DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LibraryApp.BLL.interfaces
{
    public interface IRoleService
    {
        List<RoleBLL> Get();
        void Create(string name);
        RoleBLL GetById(Guid id);
        void Delete(RoleBLL role);
        Task Edit(Guid userId, List<string> roles);

    }
}

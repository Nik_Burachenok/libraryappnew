﻿using LibraryApp.BLL.DTO.Book;
using LibraryApp.BLL.DTO.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryApp.BLL.interfaces
{
    public interface IBookService
    {
        PaginatedList<BookItem> GetOnQuery(string query, int pageNumber, int pageSize);
        PaginatedList<BookItem> Get(int pageNumber, int pageSize);
        BookItem GetById(int id);
        PaginatedList<BookItem> GetByAuthor(int authorId, int pageNumber, int pageSize);
        IEnumerable<BookItem> GetIsFavorite();
        int Create(BookItem model);
        void Delete(int id);
        void Update(BookItem model);
        void MakePopular(int bookId);
    }
}

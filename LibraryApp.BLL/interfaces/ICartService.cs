﻿using LibraryApp.BLL.DTO.Common;
using Microsoft.AspNetCore.Http;

namespace LibraryApp.BLL.interfaces
{
    public interface ICartService
    {
        Cart Get();
        Cart AddCartItem(CartItem item);
        void Delete(int bookId);
    }
}



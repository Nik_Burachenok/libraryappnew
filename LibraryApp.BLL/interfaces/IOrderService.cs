﻿using LibraryApp.BLL.DTO.Order;
using System;
using System.Collections.Generic;

namespace LibraryApp.BLL.interfaces
{
    public interface IOrderService
    {
        List<int> Create(Guid userId);
        IEnumerable<OrderBLL> Get();
        IEnumerable<OrderBLL> GetOnQuery(string query);
        void Checkout(int orderId);
        void ReturnBook(int orderId);
        IEnumerable<OrderBLL> GetCheckout();
        IEnumerable<OrderBLL> GetUnprocessed();
        IEnumerable<OrderBLL> GetReturnOrder();
        OrderBLL GetById(int id);
        IEnumerable<OrderBLL> GetListOrdersUser(Guid userId);
    }
}

﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using LibraryApp.Models;
using LibraryApp.BLL.interfaces;
using Microsoft.AspNetCore.Authorization;
using System;

namespace LibraryApp.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IBookService bookService;

        public HomeController(IBookService bookService)
        {
            this.bookService = bookService;
        }
        public IActionResult Index()
        {
            if (this.User.Identity.IsAuthenticated)
            {
                return this.RedirectToAction("MyProfile", "Home");
            }
            
            return this.LocalRedirect("/Account/Login");
        }

        public IActionResult MyProfile()
        {
            return this.View(this.bookService.GetIsFavorite());
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return this.View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? this.HttpContext.TraceIdentifier });
        }

        public IActionResult ErrorMessage(Exception ex)
        {
            return this.View(ex);
        }
    }
}

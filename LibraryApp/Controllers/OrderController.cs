﻿using LibraryApp.BLL.interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace LibraryApp.Controllers
{
    [Authorize]
    [Route("order")]
    public class OrderController : Controller
    {
        private readonly IOrderService orderService;

        public OrderController (IOrderService orderService)
        {
            this.orderService = orderService;
        }

        [HttpPost("index")]
        public IActionResult Index(Guid userId)
        {
            return this.View(this.orderService.Create(userId));
        }

        [HttpGet("get")]
        [Authorize(Roles = "admin")]
        public IActionResult Get()
        {
            return this.View(this.orderService.Get());
        }

        [HttpGet("get-on-query")]
        [Authorize(Roles = "admin")]
        public IActionResult GetOnQuery(string query)
        {
            try
            {
                return new JsonResult(this.orderService.GetOnQuery(query));
            }
            catch (ArgumentNullException)
            {
                //return this.View();
                return new JsonResult(null);
            }
            catch (Exception ex)
            {
                //return this.RedirectToAction("ErrorMessage", "Home", new { ex = ex });
                return new JsonResult(ex);
            }
        }

        [HttpPost("checkout")]
        [Authorize(Roles = "admin")]
        public IActionResult Checkout(int orderId)
        {
            this.orderService.Checkout(orderId);
            return this.RedirectToAction("Get", "Order");
        }

        [HttpPost("return-book")]
        [Authorize(Roles = "admin")]
        public IActionResult ReturnBook(int orderId)
        {
            this.orderService.ReturnBook(orderId);
            return this.RedirectToAction("Get", "Order");
        }

        [HttpGet("get-checkout")]
        [Authorize(Roles = "admin")]
        public IActionResult GetCheckout()
        {
            return this.View(this.orderService.GetCheckout());
        }

        [HttpGet("get-unprocessed")]
        [Authorize(Roles = "admin")]
        public IActionResult GetUnprocessed()
        {
            return this.View(this.orderService.GetUnprocessed());
        }

        [HttpGet("get-return-order")]
        [Authorize(Roles = "admin")]
        public IActionResult GetReturnOrder()
        {
            return this.View(this.orderService.GetReturnOrder());
        }
    }
}

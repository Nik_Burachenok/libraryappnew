﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LibraryApp.BLL.DTO.Common;
using LibraryApp.BLL.DTO.Identity;
using LibraryApp.BLL.interfaces;
using LibraryApp.DAL.Entities;
using LibraryApp.Models.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace LibraryApp.Controllers
{
    [Authorize]
    [Route("user")]
    public class UserController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly IUserService userService;
        private readonly IOrderService orderService;

        public UserController(UserManager<User> userManager, IUserService userService, IOrderService orderService)
        {
            this.userService = userService;
            this.userManager = userManager;
            this.orderService = orderService;
        }

        [HttpGet("index")]
        public ActionResult<PaginatedList<UserListModel>> Index(int pageNumber = 1, int pageSize = 12)
        {
            return this.View(this.userService.Get(pageNumber, pageSize));
        }

        [HttpGet("getJSON")]
        public ActionResult<PaginatedList<UserListModel>> GetJSON(int pageNumber, int pageSize)
        {
            return new JsonResult(this.userService.Get(pageNumber, pageSize));
        }

        [HttpGet("get-by-query")]
        public ActionResult<IEnumerable<UserListItem>> GetByQuery(string query)
        {
            return new JsonResult(this.userService.GetByQuery(query));
        }

        [HttpGet("get-by-id")]
        public async Task<IActionResult> GetById(Guid id)
        {
            this.ViewBag.Orders = this.orderService.GetListOrdersUser(id);
            return this.View(await this.userService.GetById(id));
        }

        [Authorize(Roles = "admin")]
        [HttpGet("delete")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await this.userService.Delete(id);
            return this.RedirectToAction("Index", "User");
        }

        [HttpGet("edit")]
        public async Task<IActionResult> Edit()
        {
            Guid id = Guid.Parse(this.User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);
            var user = await this.userService.GetById(id);

            if (user == null)
            {
                return this.NotFound();
            }
            return this.View(user);
        }

        [HttpPost("edit")]
        public async Task<IActionResult> Edit(UserListItem model)
        {
            if (this.ModelState.IsValid)
            {
                Guid id = Guid.Parse(this.User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);
                var user = await this.userService.GetById(id);
                if (user != null)
                {
                    user.Email = model.Email;
                    user.Name = model.Name;

                    var result = await this.userService.Edit(user);
                    if (result.Succeeded)
                    {
                        return this.RedirectToAction("GetById", "User", new { id = id });
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            this.ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }
            }
            return this.View(model);
        }

        [HttpGet("change-password")]
        public async Task<IActionResult> ChangePassword()
        {
            Guid id = Guid.Parse(this.User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);
            var user = await this.userService.GetById(id);

            if (user == null)
            {
                return this.NotFound();
            }

           UserAndPasswordModel model = new UserAndPasswordModel
            {
                Id = user.Id,
                Name = user.Name
            };

            return this.View(model);
        }

        [HttpPost("change-password")]
        public async Task<IActionResult> ChangePassword(UserAndPasswordModel model)
        {
            if (this.ModelState.IsValid)
            {
                //var user = await this.userService.GetById(model.Id);
                //if (user != null)
                //{
                try
                {
                    await this.userService.ChangePassword(model);
                    return this.RedirectToAction("GetById", "User", new { id = model.Id });
                }
                catch
                {
                    this.ModelState.AddModelError("OldPassword", "Вы ввели неверный пароль");
                }
                    
                    //if (result.Succeeded)
                    //{
                    //    return this.RedirectToAction("GetById", "User", new { id = model.Id });
                    //}
                    //else
                    //{
                    //    foreach (var error in result.Errors)
                    //    {
                    //        this.ModelState.AddModelError(string.Empty, error.Description);
                    //    }
                    //}
                //}
                //else
                //{
                //    this.ModelState.AddModelError(string.Empty, "Пользователь не найден");
                //}
            }
            return this.View(model);
        }
    }
}

﻿namespace LibraryApp.Controllers
{
    using System.Threading.Tasks;
    using LibraryApp.BLL.interfaces;
    using LibraryApp.DAL.Entities;
    using LibraryApp.Models.User;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;

    [Authorize]
    public class AccountController : Controller
    {
        private readonly IUserService userService;

        public AccountController(IUserService userService)
        {
            this.userService = userService;
        }
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Logout()
        {
            this.userService.Logout();
            return this.RedirectToAction("Index", "Home");
        }
    }
}

﻿using LibraryApp.BLL.DTO.Book;
using LibraryApp.BLL.DTO.Common;
using LibraryApp.BLL.interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace LibraryApp.Controllers
{
    [Authorize]
    [Route("book")]
    public class BookController : Controller
    {
        private readonly IBookService bookService;
        private readonly IAuthorService authorService;

        public BookController(IBookService bookService, IAuthorService authorService)
        {
            this.bookService = bookService;
            this.authorService = authorService;
        }

        [HttpGet("get")]
        public ActionResult<PaginatedList<BookItem>> Get(int pageNumber = 1, int pageSize = 12)
        {
            return this.View(this.bookService.Get(pageNumber, pageSize));
        }

        [HttpGet("getJSON")]
        public ActionResult<PaginatedList<BookItem>> GetJSON(int pageNumber, int pageSize)
        {
            return new JsonResult(this.bookService.Get(pageNumber, pageSize));
        }

        [HttpGet("get-on-query")]
        public ActionResult<PaginatedList<BookItem>> GetOnQuery (string query, int pageNumber = 1, int pageSize = 12)
        {
            return new JsonResult(this.bookService.GetOnQuery(query, pageNumber, pageSize).Items);
        }

        [HttpGet("get-by-id")]
        public IActionResult GetById(int id)
        {
            return this.View(this.bookService.GetById(id));
        }

        [HttpGet("get-by-author")]
        public ActionResult<PaginatedList<BookItem>> GetByAuthor(int authorId, int pageNumber = 1, int pageSize = 12)
        {
            return this.View(this.bookService.GetByAuthor(authorId, pageNumber, pageSize));
        }

        [Authorize(Roles = "admin")]
        [HttpGet("create")]
        public IActionResult Create()
        {
            this.ViewBag.Authors = this.authorService.GetAll();
            return this.View();
        }

        [Authorize(Roles = "admin")]
        [HttpPost("create")]
        public IActionResult Create(BookItem model)
        {
            try
            {
                var bookId = this.bookService.Create(model);
                return this.RedirectToAction("GetById", "Book", new { id = bookId });
            }
            catch
            {
                if (string.IsNullOrEmpty(model.Title))
                {
                    this.ModelState.AddModelError("Title", "Название книги не может быть пустым");
                }
                else if (string.IsNullOrEmpty(model.Isbn))
                {
                    this.ModelState.AddModelError("Isbn", "Isbn-номер книги не может быть пустым");
                }
                else if (model.AuthorItem == null)
                {
                    this.ModelState.AddModelError("AuthorItem", "Выберите автора. При отсутствии необходимого автора в перечне, создайте его модель");
                }
                else if (string.IsNullOrEmpty(model.TotalCount.ToString()))
                {
                    this.ModelState.AddModelError("TotalCount", "Поле не может быть равно нулю");
                }
                else if (string.IsNullOrEmpty(model.Count.ToString()))
                {
                    this.ModelState.AddModelError("Count", "Поле не может быть равно нулю");
                }
            }
            this.ViewBag.Authors = this.authorService.GetAll();
            return this.View();
        }

        [Authorize(Roles = "admin")]
        [HttpGet("update")]
        public IActionResult Update(int id)
        {
            this.ViewBag.Authors = this.authorService.GetAll();
            var book = this.bookService.GetById(id);
            return this.View(book);
        }

        [Authorize(Roles = "admin")]
        [HttpPost("update")]
        public IActionResult Update(BookItem model)
        {
            try
            {
                this.bookService.Update(model);
                return this.RedirectToAction("GetById", "Book", new { id = model.Id });
            }
            catch
            {
                if (string.IsNullOrEmpty(model.Title))
                {
                    this.ModelState.AddModelError("Title", "Название книги не может быть пустым");
                }
                else if (string.IsNullOrEmpty(model.Isbn))
                {
                    this.ModelState.AddModelError("Isbn", "Isbn-номер книги не может быть пустым");
                }
                else if (model.AuthorItem == null)
                {
                    this.ModelState.AddModelError("AuthorItem", "Выберите автора. При отсутствии необходимого автора в перечне, создайте его модель");
                }
                else if (string.IsNullOrEmpty(model.TotalCount.ToString()))
                {
                    this.ModelState.AddModelError("TotalCount", "Поле не может быть равно нулю");
                }
                else if (string.IsNullOrEmpty(model.Count.ToString()))
                {
                    this.ModelState.AddModelError("Count", "Поле не может быть равно нулю");
                }
            }
            this.ViewBag.Authors = this.authorService.GetAll();
            return this.View();
        }

        [Authorize(Roles = "admin")]
        [HttpGet("delete")]
        public IActionResult Delete(int id)
        {
            this.bookService.Delete(id);
            return this.RedirectToAction("Get", "Book");
        }

        [Authorize(Roles = "admin")]
        [HttpPost("make-popular")]
        public IActionResult MakePopular(int bookId)
        {
            this.bookService.MakePopular(bookId);
            return this.RedirectToAction("GetById", "Book", new { id = bookId });
        }
    }
}

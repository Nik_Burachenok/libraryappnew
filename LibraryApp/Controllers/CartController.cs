﻿using LibraryApp.BLL.DTO.Common;
using LibraryApp.BLL.interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace LibraryApp.Controllers
{
    [Authorize]
    [Route("cart")]
    public class CartController : Controller
    {
        private readonly ICartService cartService;

        public CartController(ICartService cartService)
        {
            this.cartService = cartService;
        }

        [HttpGet("index")]
        public IActionResult Index()
        {
            return this.View(this.cartService.Get());
        }

        [HttpPost("add")]
        public IActionResult Add(CartItem item)
        {
            try
            {
                this.cartService.AddCartItem(item);
            }
            catch (ArgumentOutOfRangeException)
            {
                this.ModelState.AddModelError("", "В библиотеке отсутствует указанное количество книг");
            }
            catch (ArgumentNullException)
            {
                this.ModelState.AddModelError("", "Вы ввели некорректное значение");
            }
            return this.RedirectToAction("Index", "Cart");
        }

        [HttpGet("delete")]
        public IActionResult Delete(int bookId)
        {
            this.cartService.Delete(bookId);
            return this.RedirectToAction("Index", "Cart");
        }
    }
}

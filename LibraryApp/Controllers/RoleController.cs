﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LibraryApp.BLL.DTO.Role;
using LibraryApp.BLL.interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LibraryApp.Controllers
{
    [Authorize(Roles = "admin")]
    [Route("role")]
    public class RoleController : Controller
    {
        private readonly IUserService userService;
        private readonly IRoleService roleService;

        public RoleController(IRoleService roleService, IUserService userService)
        {
            this.roleService = roleService;
            this.userService = userService;
        }

        [HttpGet("get")]
        public IActionResult Get()
        {
            return this.View(this.roleService.Get());
        }

        [HttpGet("create")]
        public IActionResult Create()
        {
            return this.View();
        }

        [HttpPost("create")]
        public IActionResult Create(string name)
        {
            try
            {
                this.roleService.Create(name);
                return this.RedirectToAction("Get", "Role");
            }
            catch (Exception ex)
            {
                return this.RedirectToAction("ErrorMessage", "Home", new { ex = ex });
            }
        }

        [HttpPost("delete")]
        public IActionResult Delete(RoleBLL role)
        {
            this.roleService.Delete(role);
            return this.RedirectToAction("Get", "Role");
        }

        [HttpGet("edit")]
        public async Task<IActionResult> Edit(Guid userId)
        {
            var user = await this.userService.GetById(userId);

            if (user != null)
            {
                var userRoles = await this.userService.GetUserRoles(userId);
                this.ViewBag.AllRoles = this.roleService.Get();
                UserRoleModel model = new UserRoleModel
                {
                    UserId = user.Id,
                    UserName = user.Name,
                    UserRoles = userRoles,
                };
                return this.View(model);
            }
            return this.NotFound();
        }

        [HttpPost("edit")]
        public async Task<IActionResult> Edit(Guid userId, List<string> roles)
        {
            await this.roleService.Edit(userId, roles);
            return this.RedirectToAction("Index", "User");
        }
    }
}

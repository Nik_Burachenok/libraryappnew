﻿using LibraryApp.BLL.DTO.Author;
using LibraryApp.BLL.DTO.Common;
using LibraryApp.BLL.interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LibraryApp.Controllers
{
    [Authorize]
    [Route("author")]
    public class AuthorController : Controller
    {
        private readonly IAuthorService authorService;

        public AuthorController(IAuthorService authorService)
        {
            this.authorService = authorService;
        }

        [HttpGet("get")]
        public ActionResult<PaginatedList<AuthorItem>> Get(int pageNumber = 1, int pageSize = 12)
        {
            return this.View(this.authorService.Get(pageNumber, pageSize));
        }

        [HttpGet("getJSON")]
        public ActionResult<PaginatedList<AuthorItem>> GetJSON(int pageNumber, int pageSize)
        {
            return new JsonResult(this.authorService.Get(pageNumber, pageSize));
        }

        [HttpGet("get-by-id")]
        public IActionResult GetById(int id)
        {
            return this.View(this.authorService.GetById(id));
        }

        [HttpGet("get-by-query")]
        public ActionResult<PaginatedList<AuthorItem>> GetByQuery(string query, int pageNumber = 1, int pageSize = 12)
        {
            return new JsonResult(this.authorService.GetByQuery(query, pageNumber, pageSize).Items);
        }

        [Authorize(Roles = "admin")]
        [HttpGet("create")]
        public IActionResult Create()
        {
            return this.View();
        }

        [Authorize(Roles = "admin")]
        [HttpPost("create")]
        public IActionResult Create(AuthorItem author)
        {
            if (!string.IsNullOrEmpty(author.Name))
            {
                var authorId = this.authorService.Create(author);
                return this.RedirectToAction("GetById", "Author", new { id = authorId });
            }
            else
            {
                this.ModelState.AddModelError("Name", "Имя автора не должно быть пустым");
            }
            return this.View();
        }

        [Authorize(Roles = "admin")]
        [HttpGet("update")]
        public IActionResult Update(int id)
        {
            return this.View(this.authorService.GetById(id));
        }

        [Authorize(Roles = "admin")]
        [HttpPost("update")]
        public IActionResult Update(AuthorItem model)
        {
            if (!string.IsNullOrEmpty(model.Name))
            {
                this.authorService.Update(model);
                return this.RedirectToAction("GetById", "Author", new { id = model.Id });
            }
            else
            {
                this.ModelState.AddModelError("Name", "Имя автора не должно быть пустым");
            }
            return this.View();
        }

        [Authorize(Roles = "admin")]
        [HttpGet("delete")]
        public IActionResult Delete(int id)
        {
            this.authorService.Delete(id);
            return this.RedirectToAction("Get", "Author");
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using LibraryApp.BLL.DTO.Identity;
using LibraryApp.BLL.DTO.User;
using LibraryApp.BLL.interfaces;
using LibraryApp.DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace LibraryApp.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly IUserService userService;

        public RegisterModel(IUserService userService)
        {
            this.userService = userService;
        }

        [Required(ErrorMessage = "Вы не ввели имя")]
        [Display(Name = "Имя")]
        [BindProperty]
        public string Name
        {
            get; set;
        }

        [Required(ErrorMessage = "Вы не ввели Email")]
        [Display(Name = "Email")]
        //[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Íåêîððåêòíûé àäðåñ ýëåêòðîííîé ïî÷òû")]
        [BindProperty]
        public string Email
        {
            get; set;
        }

        [Required(ErrorMessage = "Вы не ввели пароль")]
        [DataType(DataType.Password)]
        [StringLength(50, ErrorMessage = "Пароль должен быть не менее 6 символов и содержать прописные и заглавные буквы, цифры и символы", MinimumLength = 6)]
        [Display(Name = "Пароль")]
        [BindProperty]
        public string Password
        {
            get; set;
        }

        [Required]
        [Compare("Password", ErrorMessage = "Повторный пароль не совпадает с заданным")]
        [DataType(DataType.Password)]
        [Display(Name = "Подтвердите пароль")]
        public string PasswordConfirm
        {
            get; set;
        }

        public string ReturnUrl
        {
            get; set;
        }

        public IActionResult OnGet(string returnUrl = null)
        {
            if (this.User.Identity.IsAuthenticated)
            {
                return this.RedirectToAction("Index", "Home");
            }
            this.ReturnUrl = returnUrl;
            return this.Page();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl ??= this.Url.Content("~/");
            if (this.ModelState.IsValid)
            {
                var user = new UserBLL
                {
                    UserName = Name,
                    Email = Email,
                    Password = this.Password
                };

                var result = await this.userService.Create(user);
                await this.userService.Login(user); 
                if (result.Succeeded)
                {
                    return this.RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        this.ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return this.Page();
        }
    }
}

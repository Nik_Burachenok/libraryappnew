﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using LibraryApp.BLL.DTO.User;
using LibraryApp.BLL.interfaces;
using LibraryApp.DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace LibraryApp.Pages
{
    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        private readonly IUserService userServise;

        public LoginModel(IUserService userServise)
        {
            this.userServise = userServise;
        }

        [Required(ErrorMessage = "Вы не ввели имя")]
        [Display(Name = "Имя")]
        [BindProperty]
        public string Name
        {
            get; set;
        }

        [Required(ErrorMessage = "Вы не ввели пароль")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        [BindProperty]
        public string Password
        {
            get; set;
        }

        [BindProperty]
        public string ReturnUrl
        {
            get; set;
        }

        public IActionResult OnGet(string returnUrl = null)
        {
            returnUrl ??= this.Url.Content("~/");
            if (this.User.Identity.IsAuthenticated)
            {
                return this.RedirectToAction("Index", "Home");
            }
            this.ReturnUrl = returnUrl;
            return this.Page();
        }

        public async Task<IActionResult> OnPost(string returnUrl = null)
        {
            returnUrl ??= this.Url.Content("~/");
            if (this.ModelState.IsValid)
            {
                var result = await this.userServise.Login(new UserBLL
                {
                    UserName = this.Name,
                    Password = this.Password
                });
                //var result = await this.signInManager.PasswordSignInAsync(this.Name, this.Password, this.RememberMe, false);

                if (result.Succeeded)
                {
                    if (!string.IsNullOrEmpty(this.ReturnUrl) && this.Url.IsLocalUrl(this.ReturnUrl))
                    {
                        return this.Redirect(this.ReturnUrl);
                    }
                    else
                    {
                        return this.RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    this.ModelState.AddModelError("", "Имя или пароль введены неверно");
                }
            }
            return this.Page();
        }

    }
}

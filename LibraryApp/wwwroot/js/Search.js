﻿$(document).ready(function () {
    $("#search").keyup(function (e) {
        if (e.keyCode == 13  || e.charCode == 13) {
            return false;
        }

        $("#livesearch").html("");
        $("#livesearch").css({ "overflow": "hidden", "border": "0px", "width": "0px", "height": "0" });
        let query = $("#search").val();
        if (query.length == 0) {
            $("#livesearch").html("");
            $("#livesearch").css({ "overflow": "hidden", "border": "0px", "width": "0px", "height": "0" });
        }

        else {
            let urlAdress = $("#formSearch").attr("title") + "?query=" + query + "&pageNumber=1&pageSize=5";

            $.ajax({
                type: "GET",
                url: urlAdress,
                success: function (items) {
                    if (items.length == 0) {
                        $("#livesearch").append('<p>Результаты отсутствуют</p>');
                        $("#livesearch").css({ "overflow": "hidden", "width": "349px", "height": "35px", "padding": "6px 12px 6px 12px", "border": "solid #ced4da", "border-radius": ".25rem" });
                    }

                    for (let i = 0; i < items.length; i++) {
                        if (items.length == 1) {
                            if (urlAdress.includes("author")) {
                                $("#livesearch").append('<a id="resultSearch" href="/author/get-by-id?id=' + items[i].id + '">' + items[i].name + '</a>');
                                $("#livesearch").css({ "overflow": "hidden", "width": "349px", "height": "35px", "padding": "6px 12px 6px 12px", "border": "solid #ced4da", "border-radius": ".25rem" });
                            }
                            else if (urlAdress.includes("book")) {
                                $("#livesearch").append('<a id="resultSearch" href="/book/get-by-id?id=' + items[i].id + '">' + items[i].title + '</a>');
                                $("#livesearch").css({ "overflow": "hidden", "width": "349px", "height": "35px", "padding": "6px 12px 6px 12px", "border": "solid #ced4da", "border-radius": ".25rem" });
                            }
                        }

                        if (items.length == 2) {
                            if (i == items.length) {
                                if (urlAdress.includes("author")) {
                                    $("#livesearch").append('<a id="resultSearch" href="/author/get-by-id?id=' + items[i].id + '">' + items[i].name + '</a>');
                                    $("#livesearch").css({ "overflow": "hidden", "width": "349px", "height": "70px", "padding": "6px 12px 6px 12px", "border": "solid #ced4da", "border-radius": ".25rem" });
                                }
                                else if (urlAdress.includes("book")) {
                                    $("#livesearch").append('<a id="resultSearch" href="/book/get-by-id?id=' + items[i].id + '">' + items[i].title + '</a>');
                                    $("#livesearch").css({ "overflow": "hidden", "width": "349px", "height": "70px", "padding": "6px 12px 6px 12px", "border": "solid #ced4da", "border-radius": ".25rem" });
                                }
                            }

                            if (urlAdress.includes("author")) {
                                $("#livesearch").append('<a id="resultSearch" href="/author/get-by-id?id=' + items[i].id + '">' + items[i].name + '</a> </br>');
                                $("#livesearch").css({ "overflow": "hidden", "width": "349px", "height": "70px", "padding": "6px 12px 6px 12px", "border": "solid #ced4da", "border-radius": ".25rem" });
                                $("#livesearch").append('<hr>')
                            }
                            else if (urlAdress.includes("book")) {
                                $("#livesearch").append('<a id="resultSearch" href="/book/get-by-id?id=' + items[i].id + '">' + items[i].title + '</a> </br>');
                                $("#livesearch").css({ "overflow": "hidden", "width": "349px", "height": "70px", "padding": "6px 12px 6px 12px", "border": "solid #ced4da", "border-radius": ".25rem" });
                                $("#livesearch").append('<hr>')
                            }
                        }

                        if (items.length == 3) {
                            if (i == items.length) {
                                if (urlAdress.includes("author")) {
                                    $("#livesearch").append('<a id="resultSearch" href="/author/get-by-id?id=' + items[i].id + '">' + items[i].name + '</a>');
                                    $("#livesearch").css({ "overflow": "hidden", "width": "349px", "height": "70px", "padding": "6px 12px 6px 12px", "border": "solid #ced4da", "border-radius": ".25rem" });
                                }
                                else if (urlAdress.includes("book")) {
                                    $("#livesearch").append('<a id="resultSearch" href="/book/get-by-id?id=' + items[i].id + '">' + items[i].title + '</a>');
                                    $("#livesearch").css({ "overflow": "hidden", "width": "349px", "height": "70px", "padding": "6px 12px 6px 12px", "border": "solid #ced4da", "border-radius": ".25rem" });
                                }
                            }

                            if (urlAdress.includes("author")) {
                                $("#livesearch").append('<a id="resultSearch" href="/author/get-by-id?id=' + items[i].id + '">' + items[i].name + '</a> </br>');
                                $("#livesearch").css({ "overflow": "hidden", "width": "349px", "height": "100px", "padding": "6px 12px 6px 12px", "border": "solid #ced4da", "border-radius": ".25rem" });
                                $("#livesearch").append('<hr>')
                            }
                            else if (urlAdress.includes("book")) {
                                $("#livesearch").append('<a id="resultSearch" href="/book/get-by-id?id=' + items[i].id + '">' + items[i].title + '</a> </br>');
                                $("#livesearch").css({ "overflow": "hidden", "width": "349px", "height": "100px", "padding": "6px 12px 6px 12px", "border": "solid #ced4da", "border-radius": ".25rem" });
                                $("#livesearch").append('<hr>')
                            }
                        }

                        if (items.length > 3) {
                            if (i == items.length) {
                                if (urlAdress.includes("author")) {
                                    $("#livesearch").append('<a id="resultSearch" href="/author/get-by-id?id=' + items[i].id + '">' + items[i].name + '</a>');
                                    $("#livesearch").css({ "overflow": "hidden", "width": "349px", "height": "70px", "padding": "6px 12px 6px 12px", "border": "solid #ced4da", "border-radius": ".25rem" });
                                }
                                else if (urlAdress.includes("book")) {
                                    $("#livesearch").append('<a id="resultSearch" href="/book/get-by-id?id=' + items[i].id + '">' + items[i].title + '</a>');
                                    $("#livesearch").css({ "overflow": "hidden", "width": "349px", "height": "70px", "padding": "6px 12px 6px 12px", "border": "solid #ced4da", "border-radius": ".25rem" });
                                }
                            }

                            if (urlAdress.includes("author")) {
                                $("#livesearch").append('<a id="resultSearch" href="/author/get-by-id?id=' + items[i].id + '">' + items[i].name + '</a> </br>');
                                $("#livesearch").css({ "overflow-y": "scroll", "width": "349px", "height": "100px", "padding": "6px 12px 6px 12px", "border": "solid #ced4da", "border-radius": ".25rem" });
                                $("#livesearch").append('<hr>')
                            }
                            else if (urlAdress.includes("book")) {
                                $("#livesearch").append('<a id="resultSearch" href="/book/get-by-id?id=' + items[i].id + '">' + items[i].title + '</a> </br>');
                                $("#livesearch").css({ "overflow-y": "scroll", "width": "349px", "height": "100px", "padding": "6px 12px 6px 12px", "border": "solid #ced4da", "border-radius": ".25rem" });
                                $("#livesearch").append('<hr>')
                            }
                        }
                    }
                }
            });
        }
        
    });
});

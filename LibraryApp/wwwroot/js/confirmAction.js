﻿$.fn.simpleConfirm = function(options) {
	if (typeof options === 'undefined') options = {};

    var defaultOptions = {
	    title: 'Окно подтверждения',
	    message: '',
	    acceptBtnLabel: 'Да',
	    cancelBtnLabel: 'Нет',
	    success: function() {},
	    cancel: function() {}
	}
	options = $.extend(defaultOptions, options);

	this.each(function() {
	    var $this = $(this);
	    var html;

	    $this.addClass('simple-dialog active');

	    html = '<div class="simple-dialog-content">';
	    html += '<div class="simple-dialog-header"><h3 class="title">'+options.title+'</h3></div>';
	    html += '<div class="simple-dialog-body"><p class="message">'+options.message+'</p></div>';
	    html += '<div class="simple-dialog-footer clearfix"><a class="simple-dialog-button accept" data-action="close">'+options.acceptBtnLabel+'</a><a class="simple-dialog-button cancel" data-action="close">'+options.cancelBtnLabel+'</a></div>';
	    html += '</div>';

	    $this.html(html);

	    $(document).on('click', 'a[data-action="close"]', function(e) {
			e.preventDefault();
			$(this).parents('.simple-dialog').removeClass('active');
			if($(this).hasClass('accept')) {
				options.success();
			}
			if($(this).hasClass('cancel')) {
				options.cancel();
			}
		});
	});

	return this;
};

$("#deleteAuthor").click(function () {
    $("#myDelete").simpleConfirm({
        message: "Вы уверены?",
		success: function () {
			var url = $("#deleteAuthor").attr("data-action");
			$(location).attr('href', url);
		},
		cansel: function () {

        }
    })
})

$("#deleteBook").click(function () {
	$("#myDelete").simpleConfirm({
		message: "Вы уверены?",
		success: function () {
			var url = $("#deleteBook").attr("data-action");
			$(location).attr('href', url);
		},
		cansel: function () {

		}
	})
})

$("#deleteCart").click(function () {
	$("#myDelete").simpleConfirm({
		message: "Вы уверены?",
		success: function () {
			var url = $("#deleteCart").attr("data-action");
			$(location).attr('href', url);
		},
		cansel: function () {

		}
	})
})

$("#deleteUser").click(function () {
	$("#myDelete").simpleConfirm({
		message: "Вы уверены?",
		success: function () {
			var url = $("#deleteUser").attr("data-action");
			$(location).attr('href', url);
		},
		cansel: function () {

		}
	})
})

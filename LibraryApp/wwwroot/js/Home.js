﻿// Слайдер
autoSlider();
var strip = document.getElementById('strip');
var imgCount = document.getElementsByTagName('img').length;
strip.style.width = (200 * imgCount) + 'px'
var position = 0;

if (imgCount > 3) {
    var extremePosition = (-200) * (imgCount - 3);
}
else {
    var extremePosition = 0;
}
var extremePosition = (-200) * (imgCount - 3);
var timer;

strip.onmouseover = stopSlider;
strip.onmouseout = autoSlider;

function autoSlider() {
    timer = setTimeout(function () {
        position -= 200;
        if (position < extremePosition) {
            position = 0;
            clearTimeout(timer);
        }
        strip.style.left = position + 'px';
        autoSlider();
    }, 5000)
}

function stopSlider() {
    clearTimeout(timer);
}
﻿$(document).ready(function () {
    let htmlStart = $("#livesearch").html();
    $("#search").keyup(function (e) {
        if (e.keyCode == 13  || e.charCode == 13) {
            return false;
        }

        $("#livesearch").html("");
        let query = $("#search").val();
        if (query.length == 0) {
            $("#livesearch").html(htmlStart);
        }

        else {
            let urlAdress = $("#formSearch").attr("title") + "?query=" + query + "&pageNumber=1&pageSize=5";

            $.ajax({
                type: "GET",
                url: urlAdress,
                success: function (items) {
                    if (items.length == 0) {
                        $("#livesearch").append('<p>Ничего не найдено</p>');
                    }

                    $("#livesearch").append('<table class="table"></table>');
                    for (let i = 0; i < items.length; i++) {
                        $(".table").append('<tr><td><a href="/user/get-by-id?id=' + items[i].id +'">' + items[i].name + '</a></td><td><a class="btn btn-success" href="/role/edit?userId=' + items[i].id + '">Права доступа</a></td></tr>');
                    }
                }
            });
        }
        
    });
});

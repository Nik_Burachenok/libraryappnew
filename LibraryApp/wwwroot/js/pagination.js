﻿$(document).ready(function paginationList () {
	$("#paginationButtonPrevious").click(function (e) {
		let action = $("#paginationButtonPrevious").attr("data-action");
		let pageNumber = $("#paginationButtonPrevious").attr("data-pageNumber");
		let pageSize = $("#paginationButtonPrevious").attr("data-pageSize");
		$.ajax({
			type: "GET",
			url: action + '?pageNumber=' + pageNumber + '&pageSize=' + pageSize,
			success: function (pagination) {
				let actionGetById = $("#getById").attr("data-action");
				let rowData = AddData(pagination, actionGetById);
				let button = AddButton(pagination, action, pageSize);
				$("#data").empty();
				$("#data").append(rowData);
				$("#paginationButtonRegion").empty();
				$("#paginationButtonRegion").append(button);
				paginationList();
			}
		});
	});

	$("#paginationButtonNext").click(function (e) {
		let action = $("#paginationButtonNext").attr("data-action");
		let pageNumber = $("#paginationButtonNext").attr("data-pageNumber");
		let pageSize = $("#paginationButtonNext").attr("data-pageSize");
		$.ajax({
			type: "GET",
			url: action + '?pageNumber=' + pageNumber + '&pageSize=' + pageSize,
			success: function (pagination) {
				let actionGetById = $("#getById").attr("data-action");
				let rowData = AddData(pagination, actionGetById);
				let button = AddButton(pagination, action, pageSize);
				$("#data").empty();
				$("#data").append(rowData);
				$("#paginationButtonRegion").empty();
				$("#paginationButtonRegion").append(button);
				paginationList();
			}
		});
	});

	function AddButton(pagination, action, pageSize) {
		let button = "";
		if (pagination.hasPreviousPage) {
			button = button + '<a id="paginationButtonPrevious" data-action="' + action + '" data-pageNumber="' + (pagination.pageNumber - 1) + '" data-pageSize="' + pageSize + '" class="btn btn-outline-dark"><i class="glyphicon glyphicon-chevron-left"></i>Назад</a>';
		}

		if (pagination.hasNextPage) {
			button = button + '<a id="paginationButtonNext" data-action="' + action + '" data-pageNumber="' + (pagination.pageNumber + 1) + '" data-pageSize="' + pageSize + '" class="btn btn-outline-dark"><i class="glyphicon glyphicon-chevron-left"></i>Вперед</a>';
		}
		return button;
	}

	function AddData(pagination, actionGetById) {
		let rowData = "";

		if (actionGetById.includes("author")) {
			for (let i = 0; i < pagination.items.length; i++) {
				if (pagination.items[i].picture != null) {
					let srcValue = "data:image/jpeg;base64," + pagination.items[i].picture + "";
					rowData = rowData + '<div class="col-lg-3"><img class="img-thumbnail" src="' + srcValue + '" alt="' + pagination.items[i].name + '" title="' + pagination.items[i].name + '" style="height: 300px;" /><br /><a id="getById" href="' + actionGetById + '?id=' + pagination.items[i].id + '" data-action="' + actionGetById + '">' + pagination.items[i].name + '</a></div>';
				}
				else {
					rowData = rowData + '<div class="col-lg-3"><img class="img-thumbnail" src="" alt="' + pagination.items[i].name + '" title="' + pagination.items[i].name + '" style="height: 300px;" /><br /><a id="getById" href="' + actionGetById + '?id=' + pagination.items[i].id + '" data-action="' + actionGetById + '">' + pagination.items[i].name + '</a></div>';
				}
			}
		}

		if (actionGetById.includes("book")) {
			for (let i = 0; i < pagination.items.length; i++) {
				if (pagination.items[i].picture != null) {
					let srcValue = "data:image/jpeg;base64," + pagination.items[i].picture + "";
					rowData = rowData + '<div class="col-lg-3"><img class="img-thumbnail" src="' + srcValue + '" alt="' + pagination.items[i].title + '" title="' + pagination.items[i].title + '" style="height: 300px;" /><br /><a id="getById" href="' + actionGetById + '?id=' + pagination.items[i].id + '" data-action="' + actionGetById + '">' + pagination.items[i].title + '</a></div>';
				}
				else {
					rowData = rowData + '<div class="col-lg-3"><img class="img-thumbnail" src="" alt="' + pagination.items[i].title + '" title="' + pagination.items[i].title + '" style="height: 300px;" /><br /><a id="getById" href="' + actionGetById + '?id=' + pagination.items[i].id + '" data-action="' + actionGetById + '">' + pagination.items[i].title + '</a></div>';
				}
			}
		}

		if (actionGetById.includes("user")) {
			for (let i = 0; i < pagination.items.length; i++) {
				if (pagination.items[i].name == $("#userName").val()) {
					continue;
				}
				rowData = rowData + '<tr><td><a id="getById" href="' + actionGetById + '?id=' + pagination.items[i].id + '" data-action="' + actionGetById + '" >' + pagination.items[i].name + '</a></td><td><a class="btn btn-success" href="/role/edit?userId=' + pagination.items[i].id + '">Права доступа</a></td></tr>';
			}
		}

		return rowData;
    }
});

﻿$(document).ready(function () {
    let htmlStart = $("#livesearch").html();
    $("#search").keyup(function (e) {
        if (e.keyCode == 13  || e.charCode == 13) {
            return false;
        }

        $("#livesearch").html("");
        let query = $("#search").val();
        if (query.length == 0) {
            $("#livesearch").html(htmlStart);
        }

        else {
            let urlAdress = $("#formSearch").attr("title") + "?query=" + query;

            $.ajax({
                type: "GET",
                url: urlAdress,
                success: function (items) {
                    if (items.length == 0) {
                        $("#livesearch").html('<p>Ничего не найдено</p>');
                    }

                    $("#livesearch").append(htmlStart);
                    for (let i = 0; i < items.length; i++) {
                        if (items[i].status == 1) {
                            $("#orderList").html('<tr><td>' + items[i].id + '</td><td>' + items[i].book.title + '</td><td>' + items[i].count + '</td><td>' + items[i].book.isbn + '</td><td>' + items[i].user.name + '</td><td>' + items[i].dateOrder + '</td><td>Заказ не обработан</td><td><form action="" method="post"><input type="hidden" name="orderId" value="' + items[i].id + '" /><button class="btn btn-success" asp-action="Checkout" asp-controller="Order" type="submit">Оформить</button></form></td><td></td><td></td></tr>"');
                        }
                        else if (items[i].status == 2) {
                            $("#orderList").html('<tr><td>' + items[i].id + '</td><td>' + items[i].book.title + '</td><td>' + items[i].count + '</td><td>' + items[i].book.isbn + '</td><td>' + items[i].user.name + '</td><td>' + items[i].dateOrder + '</td><td>' + items[i].dateCheckout + '</td><td>Заказ оформлен</td><td>У пользователя</td><td><form action="" method="post"><input type="hidden" name="orderId" value="' + items[i].id + '" /><button class="btn btn-success" asp-action="ReturnBook" asp-controller="Order" type="submit">Вернуть</button></form></td></tr>"');
                        }
                        else {
                            $("#orderList").html('<tr><td>' + items[i].id + '</td><td>' + items[i].book.title + '</td><td>' + items[i].count + '</td><td>' + items[i].book.isbn + '</td><td>' + items[i].user.name + '</td><td>' + items[i].dateOrder + '</td><td>' + items[i].dateCheckout + '</td><td>Заказ оформлен</td><td>' + items[i].dateReturn + '</td><td>Книга возвращена</td></tr>"');
                        }
                    }
                }
            });
        }
        
    });
});

﻿let modal = $("#myModal");
let img = $("#myImg");
let modalImg = $("#img01");
img.click(function () {
    modal.css("display", "block");
    modalImg.attr("src", img.attr("src"));
});

let span = $("#close");
span.click(function () {
    modal.css("display", "none");
});

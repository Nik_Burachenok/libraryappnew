﻿// Добавление кнопки прокрутки вверх
window.onload = function () {
    
    window.onscroll = function () {
        updateButton();
    }

    updateButton();
}

function updateButton() {
    var scrolled = window.pageYOffset;

    if (scrolled == 0) {
        document.getElementById('top').style.display = 'none';
    }
    else {
        document.getElementById('top').style.display = 'block';
    }

    var timer;
    document.getElementById('top').onclick = function () {
        scrolled = window.pageYOffset;
        scrollToTop();
    }

    function scrollToTop() {
        if (scrolled > 0) {
            window.scrollTo(0, scrolled);
            scrolled = scrolled - 100;
            timer = setTimeout(scrollToTop, 100);
        }
        else {
            clearTimeout(timer);
            window.scrollTo(0, 0);
        }
    }
}


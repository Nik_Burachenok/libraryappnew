﻿using System;

namespace Infrastructure
{
    public enum Status
    {
        NotProcessed = 1,
        UserHas = 2,
        Returned = 3
    }
}
